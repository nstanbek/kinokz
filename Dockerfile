FROM openjdk:17

WORKDIR /kinokz

COPY target/kinokz.jar kinokz.jar

CMD ["java", "-jar", "kinokz.jar"]