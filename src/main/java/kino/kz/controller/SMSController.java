package kino.kz.controller;


import kino.kz.model.dto.SMSCreateDTO;
import kino.kz.model.dto.SMSVerifyDTO;
import kino.kz.service.AuthService;
import kino.kz.service.SMSService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sms")
@RequiredArgsConstructor
public class SMSController {

    private final SMSService smsService;
    @PostMapping()
    public ResponseEntity<Void> sendCode(SMSCreateDTO smsCreateDTO) {
        smsService.sendCode(smsCreateDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/verify")
    public ResponseEntity<Void> verifyCode(SMSVerifyDTO smsVerifyDTO) {
        smsService.verifyCode(smsVerifyDTO);
        return ResponseEntity.ok().build();
    }
}
