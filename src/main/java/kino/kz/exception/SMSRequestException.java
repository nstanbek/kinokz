package kino.kz.exception;

public class SMSRequestException extends RuntimeException{

    public SMSRequestException(String message) {
        super(message);
    }

    public SMSRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
