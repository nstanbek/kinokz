package kino.kz.model.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Table(name = "users")
@Entity
@Data
@Builder
public class User {

    @Id
    @Column(name = "id")
    private final UUID id;

    @Column(name = "username")
    private String username;
}
