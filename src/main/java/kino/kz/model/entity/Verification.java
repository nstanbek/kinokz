package kino.kz.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Data;

@Table(name = "verifications")
@Entity
@Data
@Builder
public class Verification {

    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "code")
    private Integer code;

    @Column(name = "limit")
    private Integer limit;
}
