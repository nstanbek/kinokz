package kino.kz.service;

import kino.kz.exception.SMSRequestException;
import kino.kz.model.dto.SMSCreateDTO;
import kino.kz.model.dto.SMSVerifyDTO;
import kino.kz.model.entity.Verification;
import kino.kz.util.SMSSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

@Service
@Transactional
@RequiredArgsConstructor
public class SMSService {

    private final VerificationService verificationService;

    public void sendCode(SMSCreateDTO smsCreateDTO) {
        Verification verification = Verification.builder()
                .username(smsCreateDTO.getPhoneNumber())
                .code(buildSMSCode())
                .limit(0)
                .build();

        verificationService.save(verification);

        SMSSender.sendSMS(verification);

    }


    private int buildSMSCode() {
        Random random = new Random();
        return 1000 + random.nextInt(9000);
    }

    public void verifyCode(SMSVerifyDTO smsVerifyDTO) {
        Verification verification = verificationService.findByPhoneNumber(smsVerifyDTO.getPhoneNumber());

        if(!smsVerifyDTO.getCode().equals(verification.getCode())) {
            throw new SMSRequestException("sms code is invalid");
        }

    }
}
