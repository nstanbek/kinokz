package kino.kz.service;

import kino.kz.exception.SMSRequestException;
import kino.kz.model.entity.Verification;
import kino.kz.repository.VerificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class VerificationService {

    public final VerificationRepository verificationRepository;


    @Transactional
    public void save(Verification verification) {
        verificationRepository.save(verification);
    }

    public Verification findByPhoneNumber(String phoneNumber) {
        return verificationRepository.findById(phoneNumber)
                .orElseThrow(() -> new SMSRequestException("can not send sms code"));
    }
}
