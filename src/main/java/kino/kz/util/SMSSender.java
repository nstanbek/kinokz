package kino.kz.util;

import kino.kz.model.dto.SMSCreateDTO;
import kino.kz.model.entity.Verification;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Random;

public class SMSSender {

    public final static String SMS_TOKEN = """
            kz805fa81c5c359e71657342b757c859699a40af75079b9a650f84f7e383f17f6cc228
                                            """;

    private final static String SMS_URL = """
            https://api.mobizon.kz/service/message/sendsmsmessage
                                          """;

    private final static String MESSAGE = """
            Код для активации аккаунта: %s
            """;

    public static void sendSMS(Verification verification) {
        String phoneNumber = verification.getUsername();

        try {
            HttpClient httpClient = HttpClients.createDefault();

            URIBuilder uriBuilder = new URIBuilder(SMS_URL);
            uriBuilder.setParameter("recipient", phoneNumber);
            uriBuilder.setParameter("text", String.format(MESSAGE, verification.getCode()));
            uriBuilder.setParameter("apiKey", SMS_TOKEN);


            HttpGet httpGet = new HttpGet(uriBuilder.build());

            HttpResponse httpResponse = httpClient.execute(httpGet);

            int statusCode = httpResponse.getStatusLine().getStatusCode();

            System.out.println("Response Code: "+ statusCode);

            String responseContent = EntityUtils.toString(httpResponse.getEntity());
            System.out.println("Response Content: "+responseContent);

        } catch (URISyntaxException | IOException e) {
            throw new RuntimeException(e);
        }

    }


}
